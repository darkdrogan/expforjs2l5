/* eslint-disable no-console */
const express = require('express');

const db = require('./dbConnection');

const router = express.Router();

const allComments = new Set([
  {
    // TODO: user camelCase in declaration
    idComment: 123,
    text: 'some text',
  },
  {
    idComment: 15,
    text: 'it\'s stupid, use angular, react or vue!',
  },
  {
    idComment: 22,
    text: 'hey guys u menya vsyo nice',
  },
  {
    idComment: 62,
    text: 'I\'ve become so numb I can\'t feel you there\n' +
    'I\'ve become so tired so much more aware\n' +
    'I\'m becoming this all I want to do\n' +
    'Is be more like me and be less like you\n',
  },
  {
    idComment: 74,
    text: 'ne nu a cho?',
  },
  {
    idComment: 45,
    text: 'I like express, webpack and npm',
  },
]);


// вот здесь не согласен) Каким образом эта функция должна быть function expresion оформленная как arrow function,
// а функция ниже - нет? Вообще это планировалось, как funcion declaration, так то
// если доводов нет - верну назад
const createArrayComments = (allComments) => {
  const arrayOfComments = [];
  db.selectAll().then(data => fillCommentArray(data));
  // for (let x of allComments) {
  //   arrayOfComments.push(x);
  // }
  console.log(`what the fck: ${arrayOfComments.length}`);
  return arrayOfComments;
};

function fillCommentArray(setOfComment) {
  const arrayOf = new Set();
  for(let entry of setOfComment){
    arrayOf.add(entry);
  }
  if(arrayOf.size > 0) {
    console.log(arrayOf);
    return arrayOf;
  }
}

function refreshCommentArray(commentId) {
  allComments.forEach(item => {
    if (item.idСomment === commentId){
      allComments.delete(item);
    }
  })
}

router.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  // i'll do it right later
  console.log(`Time: ${new Date().getHours()}:${new Date().getMinutes()}:${new Date().getSeconds()}:${new Date().getMilliseconds()}`);
  next();
});

// router.use(timeLogger);
// eslint-disable-next-line no-unused-vars
router.post('/add', (req, res, next) => {
  const textTo = 'Lol!';
  console.log('/add');
  db.alreadyInsert(req.body.idUser, req.body.text);
  res.json({
    result: 1,
    // окей, тернарный круче, но, он делает совершенно по другому, от того,
    // что написано в оригинале, естественно поправил
    userMessage: req.body.idUser === 1
      ? `${textTo} Whaaat? Okay, we moderate your comment:\n ${req.body.text}`
      : `${textTo}`,
  });
});

// eslint-disable-next-line no-unused-vars
router.post('/submit', (req, res, next) => {
  console.log('/submit');
  res.json({ result: 1 });
});

// eslint-disable-next-line no-unused-vars
router.post('/delete', (req, res, next) => {
  console.log('/delete');
  // console.log(req.body);
  // const commentId = req.body.idComment;
  // refreshCommentArray(commentId);
  db.deleteThis(res, req.body.idComment);
  // res.json({ result: 1 });
});

// prefer arrow functions
// eslint-disable-next-line no-unused-vars
router.post('/list', (req, res, next) => {
  // res.json({ comments: createArrayComments(allComments) })
  db.selectAll(res);
});

module.exports = router;
