'use strict';

// const promise = require('bluebird'); // or any other Promise/A+ compatible library;

// const initOptions = {
//   promiseLib: promise // overriding the default (ES6 Promise);
// };

const pgp = require('pg-promise')(/*initOptions*/);
// See also: http://vitaly-t.github.io/pg-promise/module-pg-promise.html

// Database connection details;
const cn = {
  host: 'localhost', // 'localhost' is the default;
  port: 5432, // 5432 is the default;
  database: 'garage',
  user: 'postgres',
  password: 'dark1008IFRIT'
};
// You can check for all default values in:
// https://github.com/brianc/node-postgres/blob/master/lib/defaults.js

const db = pgp(cn); // database instance;

/*db.any('CREATE TABLE cinemas (\n' +
  '        id serial,\n' +
  '        name text,\n' +
  '        location text\n' +
  ');').then(()=>console.log('allright')).catch((error)=>console.log('Error:' + error));*/
function alreadyInsert(user, text) {
  db.any(`insert into cinemas(name, location) values ('${user}', '${text}')`)
    .then(() => console.log('inserted'))
    .catch((error) => console.log('Error:' + error));
}

function selectAll(res) {
  console.log(`open the query`);
  let mySet = new Set();
  mySet = db.any('select * from cinemas')
    .then(data => res.json({comments: data}))
    .catch((error) => console.log('Error:' + error));
  return mySet;
}

function deleteThis(res, id) {
  console.log(`delete in db`);
  db.any(`delete from cinemas where ${id} = id`).then(res.json({ result: 1})).catch();
}


// NOTE: The default ES6 Promise doesn't have method `.finally`, but it is
// available within Bluebird library used here as an example.
/*

db.any('select * from users where active = $1', [true])
  .then(data => {
    console.log('DATA:', data); // print data;
  })
  .catch(error => {
    console.log('ERROR:', error); // print the error;
  })
  .finally(db.$pool.end); // For immediate app exit, shutting down the connection pool
// For details see: https://github.com/vitaly-t/pg-promise#library-de-initialization*/

exports.alreadyInsert = alreadyInsert;
exports.selectAll = selectAll;
exports.deleteThis = deleteThis;