// it's just test and stub for js2l5. I won't do good parser for this later.
// may be create express.Router();

const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const review = require('./reviewRouter');
const dbtest = require('./dbConnection');
// var multer = require('multer'); // v1.0.5
// var upload = multer(); // for parsing multipart/form-data

app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

app.use('/review', function(req, res, next){
  console.log('app here');
  next();
}, review);

app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});


app.post('/basket/get', /*upload.array(),*/function (req, res, next) {
  res.json({
    'result': 1,
    'basket': [
      {
        'id_product': 123,
        'price': 100
      }
    ],
    'amount': 100
  });
});

// just for test
app.get('/basket/add', function (req, res) {
  res.send('Hello, bitches!');
});

// upload.array() is useless for Content-type : application/json
app.post('/basket/add', /*upload.array(),*/ function (req, res, next) {
  res.json({
    'result': 1,
    'full_price': 123
  });
});

app.post('/basket/delete', function (req, res) {
  console.log(req.body);
  res.json({
    'result' : 1
  });
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000')
});
